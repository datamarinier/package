
#' Datacleaning
#'
#' Inoffensive datacleaning steps
#' Cleans names
#'  - Only letters and numbers rest replaced with '_'
#'  - all names to lower
#'  - checks validity of names
#' Cleans cells
#'  - removes all types of whitespace with NA
#'  - substitutes , for .
#'
#' @param df
#'
#' @return df
#' @export
#' @examples
#' df <- data.frame(`c 1` = 1:3, `b ` = c(1,2," "))
#' dbcleaner(df)


dbcleaner <- function(df)
{

  pb <- txtProgressBar(min = 0, max = length(names(df)), style = 3)
  for(i in 1:length(names(df)))
  {
    names(df) <- gsub('[^a-z0-9]+','_', tolower(names(df)))

    names(df) <- tolower(names(df))

    names(df) <- make.names(names(df), unique=TRUE, allow_=TRUE)

    if(class(df[[i]])[1]=="Date"){

      df[[i]] <- ifelse(grepl("^[ \t\n]*$", df[[i]]), NA,
                        ifelse(as.character(df[[i]]) == "-", NA,
                               ifelse(as.character(df[[i]]) == ".", NA,
                                      df[[i]])))
      class(df[[i]]) <- "Date"

      next()
    }

    if(class(df[[i]][1])=="POSIXct"){
      df[[i]] <- ifelse(grepl("^[ \t\n]*$", df[[i]]), NA,
                        ifelse(as.character(df[[i]]) == "-", NA,
                               ifelse(as.character(df[[i]]) == ".", NA,
                                      df[[i]])))
      class(df[[i]]) <- "POSIXct"

      next()

    }
    names(df)  <- gsub("(\\.)", "_", names(df))

    df[[i]] <- ifelse(grepl("^[ \t\n]*$", df[[i]]), NA,
                      ifelse(df[[i]] == "-", NA,
                             ifelse(df[[i]] == ".", NA,
                                    df[[i]])))

    df[[i]] <- gsub("\\s*\\([^\\)]+\\)", "", df[[i]])
    df[[i]] <- gsub(",", ".", df[[i]])
    setTxtProgressBar(pb, i)

  }
  close(pb)
  return(df)
}
